-- Buildinfo for project svec_freq
--
-- This file was automatically generated; do not edit

package buildinfo_pkg is
  constant buildinfo : string :=
       "buildinfo:1" & LF
     & "module:svec_freq" & LF
     & "commit:b59a3c2d6911e84c99d21a1e4d9fa301ffc187f4" & LF
     & "syntool:ise" & LF
     & "syndate:Tuesday, September 24 2019" & LF
     & "synauth:Tristan Gingold" & LF;
end buildinfo_pkg;
