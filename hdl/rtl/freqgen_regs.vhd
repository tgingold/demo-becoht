-- Do not edit; this file was generated by Cheby using these options:
--  --gen-hdl=freqgen_regs.vhd -i freqgen_regs.cheby
--
-- Version: 1.1.0

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.wishbone_pkg.all;

entity freqgen_regs is
  port (
    rst_n_i              : in    std_logic;
    clk_i                : in    std_logic;
    wb_i                 : in    t_wishbone_slave_in;
    wb_o                 : out   t_wishbone_slave_out;

    -- Divider register
    divider_o            : out   std_logic_vector(31 downto 0);
    divider_wr_o         : out   std_logic;

    -- Enable the generator
    control_enable_o     : out   std_logic
  );
end freqgen_regs;

architecture syn of freqgen_regs is
  signal adr_int                        : std_logic_vector(2 downto 2);
  signal rd_req_int                     : std_logic;
  signal wr_req_int                     : std_logic;
  signal rd_ack_int                     : std_logic;
  signal wr_ack_int                     : std_logic;
  signal wb_en                          : std_logic;
  signal ack_int                        : std_logic;
  signal wb_rip                         : std_logic;
  signal wb_wip                         : std_logic;
  signal divider_reg                    : std_logic_vector(31 downto 0);
  signal divider_wreq                   : std_logic;
  signal divider_wack                   : std_logic;
  signal control_enable_reg             : std_logic;
  signal control_wreq                   : std_logic;
  signal control_wack                   : std_logic;
  signal control_rint                   : std_logic_vector(31 downto 0);
  signal rd_ack_d0                      : std_logic;
  signal rd_dat_d0                      : std_logic_vector(31 downto 0);
  signal wr_req_d0                      : std_logic;
  signal wr_adr_d0                      : std_logic_vector(2 downto 2);
  signal wr_dat_d0                      : std_logic_vector(31 downto 0);
begin

  -- WB decode signals
  adr_int <= wb_i.adr(2 downto 2);
  wb_en <= wb_i.cyc and wb_i.stb;

  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        wb_rip <= '0';
      else
        wb_rip <= (wb_rip or (wb_en and not wb_i.we)) and not rd_ack_int;
      end if;
    end if;
  end process;
  rd_req_int <= (wb_en and not wb_i.we) and not wb_rip;

  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        wb_wip <= '0';
      else
        wb_wip <= (wb_wip or (wb_en and wb_i.we)) and not wr_ack_int;
      end if;
    end if;
  end process;
  wr_req_int <= (wb_en and wb_i.we) and not wb_wip;

  ack_int <= rd_ack_int or wr_ack_int;
  wb_o.ack <= ack_int;
  wb_o.stall <= not ack_int and wb_en;
  wb_o.rty <= '0';
  wb_o.err <= '0';

  -- pipelining for wr-in+rd-out
  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        rd_ack_int <= '0';
        wr_req_d0 <= '0';
      else
        rd_ack_int <= rd_ack_d0;
        wb_o.dat <= rd_dat_d0;
        wr_req_d0 <= wr_req_int;
        wr_adr_d0 <= adr_int;
        wr_dat_d0 <= wb_i.dat;
      end if;
    end if;
  end process;

  -- Register divider
  divider_o <= divider_reg;
  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        divider_reg <= "00000000000000001100001101010000";
        divider_wack <= '0';
      else
        if divider_wreq = '1' then
          divider_reg <= wr_dat_d0;
        end if;
        divider_wack <= divider_wreq;
      end if;
    end if;
  end process;
  divider_wr_o <= divider_wack;

  -- Register control
  control_enable_o <= control_enable_reg;
  process (clk_i) begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        control_enable_reg <= '0';
        control_wack <= '0';
      else
        if control_wreq = '1' then
          control_enable_reg <= wr_dat_d0(0);
        end if;
        control_wack <= control_wreq;
      end if;
    end if;
  end process;
  control_rint(0) <= control_enable_reg;
  control_rint(31 downto 1) <= (others => '0');

  -- Process for write requests.
  process (wr_adr_d0, wr_req_d0, divider_wack, control_wack) begin
    divider_wreq <= '0';
    control_wreq <= '0';
    case wr_adr_d0(2 downto 2) is
    when "0" => 
      -- divider
      divider_wreq <= wr_req_d0;
      wr_ack_int <= divider_wack;
    when "1" => 
      -- control
      control_wreq <= wr_req_d0;
      wr_ack_int <= control_wack;
    when others =>
      wr_ack_int <= wr_req_d0;
    end case;
  end process;

  -- Process for read requests.
  process (adr_int, rd_req_int, divider_reg, control_rint) begin
    -- By default ack read requests
    rd_dat_d0 <= (others => 'X');
    case adr_int(2 downto 2) is
    when "0" => 
      -- divider
      rd_ack_d0 <= rd_req_int;
      rd_dat_d0 <= divider_reg;
    when "1" => 
      -- control
      rd_ack_d0 <= rd_req_int;
      rd_dat_d0 <= control_rint;
    when others =>
      rd_ack_d0 <= rd_req_int;
    end case;
  end process;
end syn;
