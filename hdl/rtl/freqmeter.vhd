library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.wishbone_pkg.all;

entity freqmeter is
  port (
    rst_n_i : in  std_logic;
    clk_i   : in  std_logic;
    wb_i    : in  t_wishbone_slave_in;
    wb_o    : out t_wishbone_slave_out;

    inp_i   : in  std_logic;
    valid_o : out std_logic;
    count_o : out std_logic_vector(31 downto 0));
end freqmeter;


architecture arch of freqmeter is
  signal pulse : std_logic;

  signal count   : std_logic_vector(31 downto 0);
  signal counter : unsigned(31 downto 0);
  signal period  : std_logic_vector(31 downto 0);

  signal control_start : std_logic;
  signal control_wr    : std_logic;

  signal status_done : std_logic;

  signal status_wip : std_logic;
  signal status_rd  : std_logic;
begin
  inst_freqmeter_regs : entity work.freqmeter_regs
    port map (
      rst_n_i         => rst_n_i,
      clk_i           => clk_i,
      wb_i            => wb_i,
      wb_o            => wb_o,
      count_i         => count,
      period_o        => period,
      control_start_o => control_start,
      control_wr_o    => control_wr,
      status_done_i   => status_done,
      status_wip_i    => status_wip,
      status_rd_o     => status_rd);

  inst_sync : entity work.gc_sync_ffs
    generic map (
      g_sync_edge => "positive")
    port map (
      clk_i    => clk_i,
      rst_n_i  => rst_n_i,
      data_i   => inp_i,
      ppulse_o => pulse);

  process (clk_i) is
  begin
    if rising_edge (clk_i) then
      if rst_n_i = '0' then
        status_wip  <= '0';
        status_done <= '0';
        valid_o     <= '0';
      else
        if status_wip = '1' then
          if counter = unsigned(period) then
            status_wip  <= '0';
            status_done <= '1';
            valid_o     <= '1';
          else
            counter <= counter + 1;
            if pulse = '1' then
              count <= std_logic_vector(unsigned(count) + 1);
            end if;
          end if;
        else
          if control_start = '1' and control_wr = '1' then
          --  Start measure by writing '1' to start.
            count       <= (others => '0');
            counter     <= (others => '0');
            status_done <= '0';
            status_wip  <= '1';
            valid_o     <= '0';
          end if;
          if status_rd = '1' then
            status_done <= '0';
          end if;
        end if;
      end if;
    end if;
  end process;

  count_o <= count;
end arch;
