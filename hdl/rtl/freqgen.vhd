library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.wishbone_pkg.all;

entity freqgen is
  port (
    rst_n_i : in  std_logic;
    clk_i   : in  std_logic;
    wb_i    : in  t_wishbone_slave_in;
    wb_o    : out t_wishbone_slave_out;

    -- Output
    pulse_o : out std_logic
    );
end freqgen;

architecture behav of freqgen is
  signal counter        : unsigned(31 downto 0);
  signal divider        : std_logic_vector(31 downto 0);
  signal divider_wr     : std_logic;
  signal control_enable : std_logic;
  signal pulse          : std_logic;

begin

  cmp_freqgen_regs : entity work.freqgen_regs
    port map (
      rst_n_i          => rst_n_i,
      clk_i            => clk_i,
      wb_i             => wb_i,
      wb_o             => wb_o,
      divider_o        => divider,
      divider_wr_o     => divider_wr,
      control_enable_o => control_enable);

  process (clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' or control_enable = '0' or divider_wr = '1' then
        pulse   <= '0';
        counter <= (others => '0');
      else
        if counter = unsigned (divider) then
          pulse   <= not pulse;
          counter <= (others => '0');
        else
          counter <= counter + 1;
        end if;
      end if;
    end if;
  end process;

  pulse_o <= pulse;

end behav;
