Required tools to build the presentation:

* Texlive
* Latexmk
* Xelatex
* Pygments
* Make

Required tex classes/packages:

* beamer
* minted

Once everything is in place, simply do a `make`.